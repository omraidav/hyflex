# HyFlex
HyFlex (Hyper-heuristics Flexible framework) is a Java object oriented framework for the implementation and comparison of different iterative general-purpose heuristic search algorithms (also called hyper-heuristics).

See the HyFlex web site for details: http://www.asap.cs.nott.ac.uk/external/chesc2011 

The goal of this project is to collect hyper-heurisics from CHeSC 2011 (Cross-domain Heuristic Search Challenge) and enable  to reproduce the results of the challenge.

Hyper-heuristic implementers might find this environment helpful for comparing own results with other approaches.

## Build

Run the following command:
```
mvn clean package
```

## Run Competition

Run the following command:
```
java -cp hyflex-chesc-2011/target/hyflex*.jar hyflex.chesc2011.Competition -h GIHH LeanGIHH EPH PearlHunter ISEA -t 10000 -r 31
```