package hyflex.chesc2011.launcher.commands;

public abstract class Command {
  public abstract void performCommand() throws Exception;
}